PPT-Practice
=============


## Question 1: Primes!

Write a method that returns a list of all prime numbers between 2 and a given integer *N*.

## Question 2: snake_case names and camelCase names

In most programming languages variable names cannot contain spaces. If we want a variable name that consists of two or more words, we have to encode the spaces somehow. 

In this problem, we will look at two ways of doing so: *Snake Case* and *Camel Case*. 

When using Snake Case, we will use only lower case alphabets, and we just replace each space by an underscore ('_'). 

When using Camel Case, we capitalize *only* the first letter of each word except for the first word (the first word is all lower case), and then we remove all spaces.

We will also make the following assumptions (for convenience): variable names cannot contain digits or punctuation (except '\_' in the case of Snake Case), and Snake Case names cannot start with an ‘\_‘.

For example, suppose that we want to declare a variable called “Good Morning World" (quotes for clarity). In Snake Case, we would write this variable as "good_morning_world", while in Camel Case it would be "goodMorningWorld". Similarly, the string “  1 my Friendly variable  “ would become “myFriendlyVariable” in Camel Case and “my_friendly_variable” in Snake Case.

You have to implement two classes that extend a class called `VariableName`: one is `CamelCaseName` and the other is `SnakeCaseName`. Implement the constructors for these classes that take a `String` as an argument and create the appropriate variable names using the indicated conventions. Also implement the methods to convert names from one convention to the other.